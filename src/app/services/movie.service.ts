// movie.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  private movies = [
    {
      id: 1,
      title: 'Tenet',
      description:
      'Armed with only one word, Tenet, and fighting for the survival of the entire world, a Protagonist journeys through a twilight world of international espionage on a mission that will unfold in something beyond real time.',
      rating: '7.8',
      duration: '2h 30min',
      genre: ['Action', 'Sci-Fi'],
      releasedDate: '3 September 2020',
      trailerLink: 'https://www.youtube.com/embed/LdOM0x0XDMo?si=01OX8xkDJiz1n3fY',
      imageUrl: 'https://www.themoviedb.org/t/p/original/aCIFMriQh8rvhxpN1IWGgvH0Tlg.jpg',
    },
    {
      id: 2,
      title: 'Spider-Man: Into the Spider-Verse',
      description:
      'Teen Miles Morales becomes the Spider-Man of his universe, and must join with five spider-powered individuals from other dimensions to stop a threat for all realities',
      rating: '8.4',
      duration: '1h 57min',
      genre: ['Action', 'Animation', 'Adventure'],
      releasedDate: '14 December 2018',
      trailerLink: 'https://www.youtube.com/embed/tg52up16eq0?si=vvQ9777PYYoGL-4-',
      imageUrl: 'https://image.tmdb.org/t/p/original/iiZZdoQBEYBv6id8su7ImL0oCbD.jpg',
    },
    {
      id: 3,
      title: 'Knives Out',
      description:
      'A detective investigates the death of a patriarch of an eccentric, combative family',
      rating: '7.9',
      duration: '2h 10min',
      genre: ['Comedy', 'Crime', 'Drama'],
      releasedDate: '27 November 2019',
      trailerLink: 'https://www.youtube.com/embed/qGqiHJTsRkQ?si=5rOC2CitKfN4LqCe',
      imageUrl: 'https://www.themoviedb.org/t/p/original/pThyQovXQrw2m0s9x82twj48Jq4.jpg',
    },
    {
      id: 4,
      title: 'Guardians of the Galaxy',
      description:
      'A group of intergalactic criminals must pull together to stop a fanatical warrior with plans to purge the universe.',
      rating: '8.0',
      duration: '2h 1min',
      genre: ['Action', 'Adventure', 'Comedy'],
      releasedDate: ' 1 August 2014',
      trailerLink: 'https://www.youtube.com/embed/d96cjJhvlMA?si=V0NJ-yvwPUGuUBV-',
      imageUrl: 'https://www.themoviedb.org/t/p/original/ufaQyyzSUnQiqUzl4d4HmX9SoPJ.jpg',
    },
    {
      id: 5,
      title: 'Avengers: Age of Ultron',
      description:
      "When Tony Stark and Bruce Banner try to jump-start a dormant peacekeeping program called Ultron, things go horribly wrong and it's up to Earth's mightiest heroes to stop the villainous Ultron from enacting his terrible plan",
      rating: '7.3',
      duration: '2h 21min',
      genre: ['Action', 'Adventure', 'Sci-Fi'],
      releasedDate: '1 May 2015',
      trailerLink: 'https://www.youtube.com/embed/tmeOjFno6Do?si=wxShyf4rSOXXPz1r',
      imageUrl: 'https://www.themoviedb.org/t/p/original/4ssDuvEDkSArWEdyBl2X5EHvYKU.jpg',
    },
  ];

  private watchlist: string[] = [];

  constructor() {
    this.loadWatchlist();
  }

  getMovies(): any[] {
    return this.movies;
  }

  getMovieByTitle(title: string): any {
    return this.movies.find((movie) => movie.title === title);
  }

   getMovieById(id: number): any {
    return this.movies.find((movie: any) => movie.id.toString() === id);
  }  

  getWatchlist(): string[] {
    return this.watchlist;
  }

  addToWatchlist(title: string): void {
    if (!this.watchlist.includes(title)) {
      this.watchlist.push(title);
      this.saveWatchlist();
    }
  }

  removeFromWatchlist(title: string): void {
    this.watchlist = this.watchlist.filter((movie) => movie !== title);
    this.saveWatchlist();
  }

  isInWatchlist(title: string): boolean {
    return this.watchlist.includes(title);
  }

  private saveWatchlist(): void {
    localStorage.setItem('watchlist', JSON.stringify(this.watchlist));
  }

  private loadWatchlist(): void {
    const watchlist = localStorage.getItem('watchlist');
    if (watchlist) {
      this.watchlist = JSON.parse(watchlist);
    }
  }
}
