// movie-list.component.ts
import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../services/movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {
  movies: any[] = [];
  sortedMovies: any[] = [];
  watchlist: string[] = [];

  constructor(private movieService: MovieService, private router: Router) {}

  navigateToDetails(movieId: number): void {
    this.router.navigate(['/details', movieId]);
  }

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();
    this.sortedMovies = [...this.movies];
    this.watchlist = this.movieService.getWatchlist();
  }

  addToWatchlist(title: string): void {
    this.movieService.addToWatchlist(title);
    this.watchlist = this.movieService.getWatchlist();
  }

  removeFromWatchlist(title: string): void {
    this.movieService.removeFromWatchlist(title);
    this.watchlist = this.movieService.getWatchlist();
  }

  isInWatchlist(title: string): boolean {
    return this.movieService.isInWatchlist(title);
  }

  selectedSortOption:
    | 'titleA'
    | 'releasedDateH'
    | 'durationH'
    | 'ratingH'
    | 'titleZ'
    | 'releasedDateL'
    | 'durationL'
    | 'ratingL' = 'titleA';

  sortMovies(): void {
    if (this.selectedSortOption === 'titleA') {
      this.sortedMovies = [...this.movies].sort((a, b) =>
        a.title.localeCompare(b.title)
      );
    } else if (this.selectedSortOption === 'releasedDateH') {
      this.sortedMovies = [...this.movies].sort((a, b) => {
        const dateA = new Date(a.releasedDate).getTime();
        const dateB = new Date(b.releasedDate).getTime();
        return dateB - dateA;
      });
    } else if (this.selectedSortOption === 'durationH') {
      this.sortedMovies = [...this.movies].sort(
        (a, b) =>
          this.convertToMinutes(b.duration) - this.convertToMinutes(a.duration)
      );
    } else if (this.selectedSortOption === 'ratingH') {
      this.sortedMovies = [...this.movies].sort((a, b) => b.rating - a.rating);
    } else if (this.selectedSortOption === 'titleZ') {
      this.sortedMovies = [...this.movies].sort((a, b) =>
        b.title.localeCompare(a.title)
      );
    } else if (this.selectedSortOption === 'releasedDateL') {
      this.sortedMovies = [...this.movies].sort((a, b) => {
        const dateA = new Date(a.releasedDate).getTime();
        const dateB = new Date(b.releasedDate).getTime();
        return dateA - dateB;
      });
    } else if (this.selectedSortOption === 'durationL') {
      this.sortedMovies = [...this.movies].sort(
        (a, b) =>
          this.convertToMinutes(a.duration) - this.convertToMinutes(b.duration)
      );
    } else if (this.selectedSortOption === 'ratingL') {
      this.sortedMovies = [...this.movies].sort((a, b) => a.rating - b.rating);
    }
  }

  convertToMinutes(duration: string): number {
    const [hours, minutes] = duration.split(' ');
    return parseInt(hours) * 60 + parseInt(minutes);
  }

  handleImageError(event: Event) {
    console.error(
      'Error loading image:',
      (event.target as HTMLImageElement).src
    );
    (event.target as HTMLImageElement).src = 'imagen_de_reserva.jpg';
  }
}
