// movie-details.component.ts
import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../services/movie.service';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss'],
  
})
export class MovieDetailsComponent implements OnInit {
  movie: any;
  watchlist: string[] = [];

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer
    
) {}

ngOnInit() {
  this.route.paramMap.subscribe((params: any) => {
    const movieId = params.get('id');
    this.movie = this.movieService.getMovieById(movieId);
  });
}

getSafeTrailerLink(): SafeResourceUrl {
  return this.sanitizer.bypassSecurityTrustResourceUrl(this.movie.trailerLink);
}


  addToWatchlist(title: string): void {
    this.movieService.addToWatchlist(title);
    this.watchlist = this.movieService.getWatchlist();
  }

  removeFromWatchlist(title: string): void {
    this.movieService.removeFromWatchlist(title);
    this.watchlist = this.movieService.getWatchlist();
  }

  isInWatchlist(title: string): boolean {
    return this.movieService.isInWatchlist(title);
  }

  convertToMinutes(duration: string): number {
    const [hours, minutes] = duration.split(' ');
    return parseInt(hours) * 60 + parseInt(minutes);
  }

  handleImageError(event: Event) {
    console.error(
      'Error loading image:',
      (event.target as HTMLImageElement).src
    );
    (event.target as HTMLImageElement).src = 'image';
  }
}
